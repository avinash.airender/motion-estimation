def parse_obj_faces(file_path):
    #Reading the .obj file in reverse order and appending converting it to a sorted tuple because some places there is f 1 2 3 and f 1 3 2
    faces = []
    with open(file_path, 'r') as file:
        lines = file.readlines()
        for line in reversed(lines):
            if line.startswith('f '):
                face = line.strip().split()[1:]
                # Convert face vertices to sorted tuples to avoid direction issues
                face = tuple(sorted(face))
                faces.append(face)
            elif line.startswith('v '):
                break
    return faces


def count_non_identical_faces(file1, file2):
    #counting the number of identical faces by traversing through entire faces array
    faces1 = parse_obj_faces(file1)
    faces2 = parse_obj_faces(file2)

    non_identical_faces_count = 0
    total1 = len(faces1)
    total2 = len(faces2)

    print("Total number of faces in first obj file = ", total1)
    print("Total number of faces in first obj file = ", total2)

    for face1 in faces1:
        #print(face1)
        if face1 not in faces2:
            non_identical_faces_count += 1


    return non_identical_faces_count


# File paths for the two .obj files
file_path1 = r"E:\aiRender\OBJ_face\OBJ_face\frame1.obj"
file_path2 = r"E:\aiRender\OBJ_face\OBJ_face\frame10.obj"


non_identical_faces = count_non_identical_faces(file_path1, file_path2)

print("Number of non identical faces = ", non_identical_faces)
