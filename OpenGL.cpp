#include <GL/glew.h> //This handles all openGl function pointers
#include <GLFW/glfw3.h> //Create window and handle input
#include <iostream>

// Function to initialize GLFW and create a window
GLFWwindow* initOpenGL() 
{
    if (!glfwInit()) 
    {
        std::cerr << "Failed to initialize GLFW" << std::endl;
        return nullptr;
    }

    //3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); 
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); 
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //Only modern OpenGL features are available (in mordern openGL, the shaders have access to the GPU)

    GLFWwindow* window = glfwCreateWindow(800, 600, "OpenGL Test Window", nullptr, nullptr); //Creates the 800x600 window
    //The first null pointer is used to create windowed mode, if a valid GLFWmonitor* is passed the window will be created in full screen mode on that screen
    //Second null pointer is used to ensure that the window has its own openGL properties and does not share resources with any other window
    if (!window) 
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return nullptr;
    }

    glfwMakeContextCurrent(window); //Creating context for current window

    // Initialize GLEW
    if (glewInit() != GLEW_OK) 
    {
        std::cerr << "Failed to initialize GLEW" << std::endl;
        glfwDestroyWindow(window);
        glfwTerminate();
        return nullptr;
    }
    
    return window;
}

int main() 
{
    GLFWwindow* window = initOpenGL();
    if (!window) 
    {
        return -1;
    }

    // Main loop
    while (!glfwWindowShouldClose(window)) 
    {
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT);//Clears the screen with the color specified by glClearColor. By default, this is black.

        // Swap buffers
        glfwSwapBuffers(window); //Swaps the front and back buffers, displaying the rendered frame.
        glfwPollEvents(); //Processes all pending events, such as keyboard and mouse input.

    }

    // Clean up
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

